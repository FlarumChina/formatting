<?php

/*
 * This file is part of fof/formatting.
 *
 * Copyright (c) 2019 FriendsOfFlarum.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace FoF\Formatting\Listeners;

use Flarum\Api\Event\Serializing;
use Flarum\Api\Serializer\ForumSerializer;
use Flarum\Formatter\Event\Configuring;
use Flarum\Settings\SettingsRepositoryInterface;
use Illuminate\Contracts\Events\Dispatcher;
use s9e\TextFormatter\Configurator\Bundles\MediaPack;

class FormatterConfigurator
{
    private $plugins = [
        'Autoimage',
        'Autovideo',
        'FancyPants',
        'HTMLEntities',
        'MediaEmbed',
        'PipeTables',
    ];

    /**
     * @var SettingsRepositoryInterface
     */
    private $settings;

    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Subscribes to the Flarum events.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(Serializing::class, [$this, 'addData']);
        $events->listen(Configuring::class, [$this, 'configureFormatter']);
    }

    /**
     * Adds settings to admin settings.
     *
     * @param Serializing $event
     */
    public function addData(Serializing $event)
    {
        if ($event->isSerializer(ForumSerializer::class) && $event->actor->isAdmin()) {
            $event->attributes['fof-formatting.plugins'] = $this->plugins;
        }
    }

    public function configureFormatter(Configuring $event)
    {
        foreach ($this->plugins as $plugin) {
            $enabled = $this->settings->get('fof-formatting.plugin.' . strtolower($plugin));

            if ($enabled) {
                if ($plugin == 'MediaEmbed') {
                    $event->configurator->MediaEmbed->add(
                        'music163',
                        [
                            'host' => 'music.163.com',
                            'extract' => '!music\\.163\\.com/#/(?\'mode\'song|album|playlist|dj)\\?id=(?\'id\'\\d+)!',
                            'choose' => [
                                'when' => [
                                    [
                                        'test' => '@mode = \'album\'',
                                        'iframe' => [
                                            'width' => 380,
                                            'height' => 450,
                                            'src' => '//music.163.com/outchain/player?type=1&id={@id}&auto=0&height=450'
                                        ]
                                    ],
                                    [
                                        'test' => '@mode = \'song\'',
                                        'iframe' => [
                                            'width' => 380,
                                            'height' => 86,
                                            'src' => '//music.163.com/outchain/player?type=2&id={@id}&auto=0&height=66'
                                        ]
                                    ],
                                    [
                                        'test' => '@mode = \'dj\'',
                                        'iframe' => [
                                            'width' => 380,
                                            'height' => 86,
                                            'src' => '//music.163.com/outchain/player?type=3&id={@id}&auto=0&height=66'
                                        ]
                                    ],
                                ],
                                'otherwise' => [
                                    'iframe' => [
                                        'width' => 380,
                                        'height' => 450,
                                        'src' => '//music.163.com/outchain/player?type=0&id={@id}&auto=0&height=450'
                                    ]
                                ]
                            ]
                        ]
                    );
                    $event->configurator->MediaEmbed->add(
                        'bilibili',
                        [
                            'host'	  => ['bilibili.com','b23.tv',],
                            'extract' => [
                                "!bilibili\.com/video(/av(?'aid'[-0-9]+))|(/BV(?'bvid'[-0-9A-Z_a-z]+))(\?p=(?'pn'[-0-9]+))?!",
                                "!b23\.tv/(/av(?'aid'[-0-9]+))|(/BV(?'bvid'[-0-9A-Z_a-z]+))(/p(?'pn'[-0-9]+))?!"
                            ],
                            'iframe' => [
                                'src'  => '//player.bilibili.com/player.html?aid={@aid}&bvid={@bvid}&page={@pn}'
                            ]
                        ]
                    );
                    $event->MediaEmbed->add(
                        'acfun',
                        [
                            'host'	  => 'acfun.cn',
                            'extract' => "!acfun\.cn/v/ac(?'acid'[-0-9]+)!",
                            'iframe' => [
                                'src'  => '//www.acfun.cn/player/ac{@acid}'
                            ]
                        ]
                    );
                    $event->MediaEmbed->add(
                        'niconico',
                        [
                            'host'	  => ['nicovideo.jp','nico.ms'],
                            'extract' => [
                                "!nicovideo\.jp/watch/sm(?'smid'[-0-9]+)!",
                                "!nico\.ms/sm(?'smid'[-0-9]+)!",
                            ],
                            'iframe' => [
                                'src'  => '//embed.nicovideo.jp/watch/sm{@smid}'
                            ]
                        ]
                    );
                    $event->configurator->MediaEmbed->add(
                        'qq',
                        [
                            'host' => 'qq.com',
                            'extract' => [
                                "!qq\\.com/x/cover/\\w+/(?'id'\\w+)\\.html!",
                                "!qq\\.com/x/cover/\\w+\\.html\\?vid=(?'id'\\w+)!",
                                "!qq\\.com/cover/[^/]+/\\w+/(?'id'\\w+)\\.html!",
                                "!qq\\.com/cover/[^/]+/\\w+\\.html\\?vid=(?'id'\\w+)!",
                                "!qq\\.com/x/page/(?'id'\\w+)\\.html!",
                                "!qq\\.com/page/[^/]+/[^/]+/[^/]+/(?'id'\\w+)\\.html!"
                            ],
                            'iframe' => [
                                'width' => 760,
                                'height' => 450,
                                'src' => 'https://v.qq.com/txp/iframe/player.html?vid={@id}&tiny=0&auto=0'
                            ]
                        ]
                    );
                    (new MediaPack)->configure($event->configurator);
                } else {
                    $event->configurator->$plugin;
                }
            }
        }
    }
}
